export interface IUser {
    id: string;
    name: string; 
    verified: boolean; 
    contactNumber: string; 
    email: string; 
}