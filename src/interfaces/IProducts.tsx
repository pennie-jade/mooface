export interface IProducts {
    id: string;
    colors: string[];
    customiseable: boolean; 
    description: string; 
    name: string; 
    price: number;
    quantity: number; 
}