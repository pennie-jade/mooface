import { useContext } from "react";
import { Col, Container } from "react-bootstrap";
import { CartContext } from "../../contexts/ShoppingCart";
import { IProducts } from "../../interfaces/IProducts";
import "../Product/Product.css";
import Button from "../Button/Button";

interface Props {
    product: IProducts;
}

const Product = (props: Props) => {
    const { addProductToCart } = useContext(CartContext);

    return (
        <Col className='product-container' xs={12} sm={6} md={6} lg={6} xl={4}>
            <Container className='product' fluid>
                <h4>{props.product.name}</h4>
                <h6>£{props.product.price}</h6>
                <Button
                    text={"Add to Bag"}
                    color={"#2D3047"}
                    onClick={addProductToCart.bind(
                        this,
                        props.product
                    )}></Button>
            </Container>
        </Col>
    );
};

export default Product;
