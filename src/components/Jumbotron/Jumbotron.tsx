import { Container } from "react-bootstrap";
import "../Jumbotron/Jumbotron.css";
import Button from "../Button/Button";

interface Props {
    header: string;
    description: string;
    button: boolean;
}

const Jumbotron = (props: Props) => {
    return (
        <Container className='jumbotron'>
            <h1>{props.header}</h1>
            <h4>{props.description}</h4>
            {props.button === true && (
                <Button text={"Learn More"} color={"#EF626C"} />
            )}
        </Container>
    );
};

export default Jumbotron;
