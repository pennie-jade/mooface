import { IconDefinition } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "react-bootstrap";
import "../Button/Button.css";

interface Props {
    children?: any;
    icon?: IconDefinition;
    text: string;
    onClick?: any;
    color: string;
    href?: string;
    center?: boolean;
    fullWidth?: boolean;
}

const Button = (props: Props) => {
    return (
        <a className='wrap' href={props.href}>
            <button
                style={{ backgroundColor: props.color }}
                className={`custom_button ${!!props.fullWidth ? 'full_width' : ''}`}
                onClick={props.onClick}>
                <Row className='d-flex justify-content-center'>
                    {props.icon && (
                        <Col className='col' xs={2}>
                            <FontAwesomeIcon icon={props.icon} />
                        </Col>
                    )}
                    <Col xs={'auto'}>
                        {props.text}
                    </Col>
                </Row>
            </button>
        </a>
    );
};

export default Button;
