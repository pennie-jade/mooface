import { useContext } from "react";
import { Nav } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import { Link, Routes, Route } from "react-router-dom";
import Dashboard from "../../auth/Dashboard/Dashboard";
import { UserContext } from "../../contexts/User";
import Register from "../../auth/Register/Register";
import Reset from "../../auth/Reset/Reset";
import Login from "../../screens/Login";
import "../Navigation/Navigation.css";
import Home from "../../screens/Home";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignIn } from "@fortawesome/free-solid-svg-icons";
import Products from "../../screens/Products";
import Cart from "../../screens/Cart";

interface Props {
    cartItemNumber: number;
}

const Navigation = (props: Props) => {
    const { user } = useContext(UserContext);
    return (
        <div>
            <Navbar collapseOnSelect expand='md' variant='dark' className='bg'>
                <Container className='font-lato-regular text' fluid>
                    <Navbar.Brand href='#home'>Mooface Designs</Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-nav'>
                        <Nav className='me-auto'>
                            <Nav.Link href='/'>Home</Nav.Link>
                            <Nav.Link href='products'>Products</Nav.Link>
                            <Nav.Link href='contact'>Contact</Nav.Link>
                            <Nav.Link href='faq'>FAQ</Nav.Link>
                        </Nav>
                        <Nav>
                            <Nav.Link href='cart'>
                                Cart <strong>{props.cartItemNumber}</strong>
                            </Nav.Link>
                            {user?.id === undefined ? (
                                <Nav.Link href='login'>
                                    <FontAwesomeIcon
                                        className='login-icon'
                                        icon={faSignIn}
                                        size='lg'
                                        color='#CCCCCC'
                                    />
                                    Login
                                </Nav.Link>
                            ) : (
                                <Nav.Link>
                                    Signed in:{" "}
                                    <Link to='dashboard'>{user?.name}</Link>
                                </Nav.Link>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <div className='main'>
                {/* Define all the routes */}
                <Routes>
                    <Route path='/' element={<Home />}></Route>
                    <Route path='cart' element={<Cart />}></Route>
                    <Route path='products' element={<Products />}></Route>
                    <Route path='contact' element={<Contact />}></Route>
                    <Route path='faq' element={<FAQ />}></Route>
                    <Route path='register' element={<Register />} />
                    <Route path='reset' element={<Reset />} />
                    <Route path='login' element={<Login />}></Route>
                    <Route path='dashboard' element={<Dashboard />} />
                    <Route path='*' element={<NotFound />}></Route>
                </Routes>
            </div>
        </div>
    );
};

// export const Products = () => {
//     return <div>This is the products page</div>;
// };
export const Contact = () => {
    return <div>This is the page where you put a contact form</div>;
};
export const FAQ = () => {
    return <div>This is the page where you put FAQ's</div>;
};
export const NotFound = () => {
    return <div>This is a 404 page</div>;
};

export default Navigation;
