import {
    createUserWithEmailAndPassword,
    getAuth,
    GoogleAuthProvider,
    sendPasswordResetEmail,
    signInWithEmailAndPassword,
    signInWithPopup,
    signOut,
} from "firebase/auth";
import { createContext, useCallback, useEffect, useState } from "react";
import { IUser } from "../interfaces/IUser";
import { app, db } from "../auth/Firebase";
import { addDoc, collection, getDocs, query, where } from "firebase/firestore";

export type UserContextData = {
    user: IUser | null;
    login: (email: string, password: string) => Promise<string>;
    signInWithGoogle: () => void;
    logout: () => void;
    registerUser: (name: any, email: string, password: string) => void;
    sendPasswordReset: (email: string) => void;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const UserContextDefaultValue: UserContextData = {
    user: null,
    login: async () => "",
    signInWithGoogle: async () => null,
    logout: () => null,
    registerUser: () => null,
    sendPasswordReset: () => null,
};

export const UserContext = createContext<UserContextData>(
    UserContextDefaultValue
);

export function useUserContextValue(): UserContextData {
    const auth = getAuth(app);
    const googleProvider = new GoogleAuthProvider();
    const [user, setUser] = useState<IUser | null>(null);
    const [registerWithEmail] = useState({});
    const [loginWithEmail] = useState({});
    const [loginWithGoogle] = useState({});

    useEffect(() => {
        const user = auth.onAuthStateChanged((user) => {
            setUser({
                id: user?.uid!,
                name: user?.displayName!,
                verified: user?.emailVerified!,
                contactNumber: user?.phoneNumber!,
                email: user?.email!,
            });
            //setLoading(false)
        });
        return user;
    }, [auth]);

    const registerUser = useCallback(
        (name: string, email: string, password: string) => {
            return createUserWithEmailAndPassword(auth, email, password)
                .then(async (res) => {
                    const user = res.user;
                    await addDoc(collection(db, "users"), {
                        id: user.uid,
                        displayName: name,
                        authProvider: "local",
                        email: email,
                    });
                })
                .catch((error) => {
                    const errorMessage = error.message;
                    return "Error: " + errorMessage;
                });
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [registerWithEmail]
    );

    const sendPasswordReset = async (email: string) => {
        try {
            await sendPasswordResetEmail(auth, email);
            alert("Password reset link sent!");
        } catch (err: any) {
            console.error(err);
            alert(err.message);
        }
    };

    const login = useCallback(
        (email: string, password: string) => {
            return signInWithEmailAndPassword(auth, email, password)
                .then((userCredential) => {
                    const user = userCredential.user;
                    return "Success" + user.uid;
                })
                .catch((error) => {
                    const errorMessage = error.message;
                    return "Error: " + errorMessage;
                });
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [loginWithEmail]
    );

    const signInWithGoogle = useCallback(
        async () => {
            const res = await signInWithPopup(auth, googleProvider);
            const user = res.user;
            const q = query(
                collection(db, "users"),
                where("uid", "==", user.uid)
            );
            const docs = await getDocs(q);
            if (docs.docs.length === 0) {
                await addDoc(collection(db, "users"), {
                    id: user.uid,
                    name: user.displayName,
                    authProvider: "google",
                    email: user.email,
                });
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [loginWithGoogle]
    );

    const logout = () => {
        signOut(auth);
    };

    return {
        user,
        login,
        signInWithGoogle,
        logout,
        registerUser,
        sendPasswordReset,
    };
}
