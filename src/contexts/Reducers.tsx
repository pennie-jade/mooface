import { ICart } from "../interfaces/ICart";
import { IProducts } from "../interfaces/IProducts";

export const ADD_PRODUCT = "ADD_PRODUCT";
export const REMOVE_PRODUCT = "REMOVE_PRODUCT";

const addProductToCart = (product: IProducts, state: { cart: any }) => {
    // Search if item is already in cart by item id
    const inCart = state.cart.some(
        (item: { id: string }) => item.id === product.id
    );

    if (inCart) {
        // Already in cart, shallow copy cart items
        return {
            ...state,
            cart: state.cart.map((item: { id: string; quantity: number }) =>
                item.id === product.id
                    ? {
                          // Found item, shallow copy item and update quantity property
                          ...item,
                          quantity: item.quantity + 1,
                      }
                    : item
            ),
        };
    } else {
        return {
            ...state,
            cart: [
                // Shallow copy cart items
                ...state.cart,
                // Add new cart item
                {
                    id: product.id,
                    quantity: 1,
                    name: product.name,
                    price: product.price,
                },
            ],
        };
    }
};

const removeProductFromCart = (product_id: string, state: { cart: any }) => {
    const updatedCart = [...state.cart];

    const updatedItemIndex = updatedCart.findIndex(
        (item) => item.id === product_id
    );

    const updatedItem = {
        ...updatedCart[updatedItemIndex],
    };   
    updatedItem.quantity--;

    if (updatedItem.quantity <= 0) {
        updatedCart.splice(updatedItemIndex, 1);
    } else {
        updatedCart[updatedItemIndex] = updatedItem;
    }

    return { ...state, cart: updatedCart };
};

export const shopReducer = (
    state: { cart: ICart },
    action: { type: string; product: IProducts | null; product_id: string }
) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return addProductToCart(action.product!, state);
        case REMOVE_PRODUCT:
            return removeProductFromCart(action.product_id, state);
        default:
            return state;
    }
};
