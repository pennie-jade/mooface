import { collection, getDocs } from "firebase/firestore";
import {
    createContext,
    useCallback,
    useEffect,
    useReducer,
    useState,
} from "react";
import { db } from "../auth/Firebase";
import { ICart } from "../interfaces/ICart";
import { IProducts } from "../interfaces/IProducts";
import { shopReducer, ADD_PRODUCT, REMOVE_PRODUCT } from "./Reducers";

export type CartContextData = {
    cart: ICart[];
    fetchProducts: () => any;
    addProductToCart: (product: any) => void;
    removeProductFromCart: (productId: any) => void;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const CartContextDefaultValue: CartContextData = {
    cart: [],
    fetchProducts: () => null,
    addProductToCart: () => null,
    removeProductFromCart: () => null,
};

export const CartContext = createContext<CartContextData>(
    CartContextDefaultValue
);

export function useCartContextValue(): CartContextData {
    const [getProducts] = useState({});
    // Get local storage for the shopping cart
    const localState = JSON.parse(localStorage.getItem("cart")!);
    const [cartState, dispatch] = useReducer(shopReducer, { cart: localState || [] });

    // Persist state on each update
    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cartState.cart));
    }, [cartState.cart]);

    const addProductToCart = (product: IProducts) => {
        setTimeout(() => {
            dispatch({
                type: ADD_PRODUCT,
                product: product,
                product_id: "",
            });
        }, 500);
    };

    const removeProductFromCart = (product_id: string) => {
        setTimeout(() => {
            dispatch({
                type: REMOVE_PRODUCT,
                product_id: product_id,
                product: null,
            });
        }, 500);
    };

    const fetchProducts = useCallback(() => {
        return getDocs(collection(db, "products"))
            .then((collections) => {
                const listOfProducts: any[] = [];
                collections.forEach((doc) => {
                    listOfProducts.push({
                        id: doc.id,
                        colors: doc.get("colors"),
                        customiseable: doc.get("customiseable"),
                        description: doc.get("description"),
                        name: doc.get("name"),
                        price: doc.get("price"),
                        quantity: doc.get("quantity"),
                    });
                });
                return listOfProducts;
            })
            .catch((error) => {
                const errorMessage = error.message;
                console.log(errorMessage);
                return errorMessage;
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [getProducts]);

    return {
        cart: cartState.cart,
        fetchProducts,
        addProductToCart,
        removeProductFromCart,
    };
}
