const colors = {
    onyx: '#393D3F',
    teal: "#62929E",
    gray: "#F4F4ED",
    blue: "#546A7B",
    orange: "#DA7635"
} as const;

export default colors; 