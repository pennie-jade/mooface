//import { faKey } from "@fortawesome/free-solid-svg-icons";
import { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Button from "../../components/Button/Button";
import { UserContext } from "../../contexts/User";
import "../Reset/Reset.css";

function Reset() {
    const { sendPasswordReset, user } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        // if (loading) return;
        if (user) navigate("/dashboard");
    }, [user, navigate]);

    return (
        <div className='reset'>
            <div className='reset__container'>
                <input
                    type='text'
                    className='reset__textBox'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='E-mail Address'
                />
                <Button
                    text={"Send Password Reset"}
                    fullWidth={true}
                    onClick={() => sendPasswordReset(email)}
                    color={"#312F2F"}
                />
                <div>
                    Don't have an account? <Link className='link-color' to='/register'>Register</Link>{" "}
                    now.
                </div>
            </div>
        </div>
    );
}
export default Reset;
