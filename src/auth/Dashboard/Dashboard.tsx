/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "../Dashboard/Dashboard.css";
import { UserContext } from "../../contexts/User";

function Dashboard() {
    const { user, logout } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (user?.id === undefined) return navigate("/");
    }, [user, navigate]);

    return (
        <div className='dashboard'>
            <div className='dashboard__container'>
                Logged in as
                <div>{user?.name}</div>
                <br/>
                <div>{user?.email}</div>
                <button className='dashboard__btn' onClick={logout}>
                    Logout
                </button>
            </div>
        </div>
    );
}
export default Dashboard;
