//import { faGoogle } from "@fortawesome/free-brands-svg-icons";
//import { faLock } from "@fortawesome/free-solid-svg-icons";
import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Button from "../../components/Button/Button";
import { UserContext } from "../../contexts/User";
import "../Register/Register.css";

const Register = () => {
    const { registerUser, user, signInWithGoogle } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");
    const navigate = useNavigate();

    const register = () => {
        if (!name) alert("Please enter name");
        registerUser(name, email, password);
    };

    useEffect(() => {
        // if (loading) return;
        if (user) navigate("/dashboard");
    }, [navigate, user]);

    return (
        <div className='register'>
            <div className='register__container'>
                <input
                    type='text'
                    className='register__textBox'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    placeholder='Full Name'
                />
                <input
                    type='text'
                    className='register__textBox'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='E-mail Address'
                />
                <input
                    type='password'
                    className='register__textBox'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder='Password'
                />
                <Button
                    text={"Register"}
                    fullWidth={true}
                    onClick={register}
                    color={"#312F2F"}
                />
                <Button
                    text={"Register with Google"}
                    fullWidth={true}
                    onClick={signInWithGoogle}
                    color={"#EF626C"}
                />
                <div>
                    Already have an account? <Link className='link-color' to='/login'>Login</Link> now.
                </div>
            </div>
        </div>
    );
}
export default Register;
