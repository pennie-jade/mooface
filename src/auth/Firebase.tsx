import { getFunctions, httpsCallable } from "firebase/functions";
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import {loadStripe} from '@stripe/stripe-js';

const firebaseConfig = {
    apiKey: "AIzaSyCpgm_VlS59KzWQ1h98gcf4UIAvQ1zWSLY",
    authDomain: "mooface-designs.firebaseapp.com",
    projectId: "mooface-designs",
    storageBucket: "mooface-designs.appspot.com",
    messagingSenderId: "689799884847",
    appId: "1:689799884847:web:8d8fb1d7ec33592db156da",
    measurementId: "G-L4PDX37H0J",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

//const createStripeCheckout = firebase.functions().httpsCallable('createStripeCheckout');

const processPayment = async () => {
    console.log("Clicked!")
    const functions = getFunctions(); 
    const callableReturnMessage = httpsCallable(functions, 'createStripeCheckout');
    const stripe = await loadStripe('pk_test_51MdDFQIrHI58dE3omrmOW4O25vJL6wnyCbcB5S76p8tGek4rqx297n34d72dzlMmBNfxLNtOyhKNy65XLGenhrB400zgYmBXJ4', {
        apiVersion: '2022-11-15',
      });

    callableReturnMessage().then((result: any) => {
        console.log(result);
        const sessionId = result.data.id; 
        stripe!.redirectToCheckout({ sessionId: sessionId})
      }).catch((error) => {
        console.log(`error: ${JSON.stringify(error)}`);
      });
}

export { app, db, processPayment };
