import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import Navigation from "./components/Navigation/Navigation";
import { CartContext, useCartContextValue } from "./contexts/ShoppingCart";
import { UserContext, useUserContextValue } from "./contexts/User";
import { ICart } from "./interfaces/ICart";

function App() {
    const userContextValue = useUserContextValue();
    const cartContextValue = useCartContextValue();

    return (
        <UserContext.Provider value={userContextValue}>
            <CartContext.Provider value={cartContextValue}>
                <Router>
                    <Navigation
                        cartItemNumber={cartContextValue.cart.reduce(
                            (
                                count,
                                product: ICart
                            ) => {
                                return count + product.quantity;
                            },
                            0
                        )}
                    />
                </Router>
            </CartContext.Provider>
        </UserContext.Provider>
    );
}

export default App;
