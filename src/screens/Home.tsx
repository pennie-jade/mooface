import {
    faFacebook,
    faInstagram,
    faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { faBookmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container } from "react-bootstrap";
import Button from "../components/Button/Button";
import "../styles/Home.css";

const Home = () => {
    return (
        <Container fluid className='home'>
            <section className='showcase'>
                <div className='video-container'>
                    <video
                        src='https://traversymedia.com/downloads/video.mov'
                        autoPlay
                        muted
                        loop></video>
                </div>
                <div className='content'>
                    <h1>Mooface Design</h1>
                    <h3>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Cras semper auctor neque vitae tempus.
                        Neque gravida in fermentum et sollicitudin. Cursus vitae
                        congue mauris rhoncus aenean vel elit. A condimentum
                        vitae sapien pellentesque habitant morbi tristique.
                        Felis donec et odio pellentesque diam.
                    </h3>
                    <Button
                        href='#about'
                        center={true}
                        text={"Read More"}
                        icon={faBookmark}
                        color={"#EF626C"}
                    />
                </div>
            </section>

            <section id='about'>
                <h1>About</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. At imperdiet dui accumsan sit amet nulla facilisi
                    morbi. Dictum sit amet justo donec, magna ac placerat
                    vestibulum lectus mauris ultrices eros. Ac tortor vitae
                    purus faucibus ornare suspendisse sed nisi lacus. Aliquet
                    lectus proin nibh nisl condimentum id venenatis a
                    condimentum. Id nibh tortor id aliquet lectus proin nibh
                    nisl. Donec ac odio tempor orci dapibus ultrices. Metus
                    vulputate eu scelerisque felis imperdiet proin fermentum leo
                    vel. Leo integer malesuada nunc vel risus.
                </p>

                <h2>Follow Us!</h2>
                <div className='social'>
                    <FontAwesomeIcon className='social__icon' icon={faFacebook} size='3x' />
                    <FontAwesomeIcon className='social__icon' icon={faInstagram} size='3x' />
                    <FontAwesomeIcon className='social__icon' icon={faTwitter} size='3x' />
                </div>
            </section>
        </Container>
    );
};

export default Home;
