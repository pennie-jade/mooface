import { useContext, useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { processPayment } from "../auth/Firebase";
import Button from "../components/Button/Button";
import { CartContext } from "../contexts/ShoppingCart";

const Cart = () => {
    const { cart, removeProductFromCart } = useContext(CartContext);
    const [total, setTotal] = useState<number>(0);

    useEffect(() => {
        const initialValue = 0;
        setTotal(
            cart.reduce(
                (total, item) => total + item.price * item.quantity,
                initialValue
            )
        );
    }, [cart, total]);

    return (
        <Container fluid>
            <Row>
                <Col>
                    {cart.length <= 0 && <p>No Item in the Cart!</p>}
                    {cart.map((cartItem: any) => (
                        <div key={cartItem.id}>
                            <strong>{cartItem.name}</strong> - £{cartItem.price}{" "}
                            ({cartItem.quantity})
                            <div
                                onClick={removeProductFromCart.bind(
                                    this,
                                    cartItem.id
                                )}>
                                Remove
                            </div>
                        </div>
                    ))}
                </Col>
                <Col>
                    <h4>TOTAL</h4>
                    <Row>
                        <Col>Sub-total</Col>
                        <Col>
                            <h6>£{total}</h6>
                        </Col>
                    </Row>
                    <Row>
                        <Col>Delivery</Col>
                        <Col>
                            <h6>£6</h6>
                        </Col>
                    </Row>
                    <Button
                        onClick={processPayment}
                        text={"CHECKOUT"}
                        color={"#EF626C"}
                        fullWidth></Button>
                </Col>
            </Row>
        </Container>
    );
};

export default Cart;
