//import { faGoogle } from "@fortawesome/free-brands-svg-icons";
//import { faLock } from "@fortawesome/free-solid-svg-icons";
import { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../contexts/User";
import "../styles/Login.css";
import Button from "../components/Button/Button";

function Login() {
    const { user, login, signInWithGoogle } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        // if (loading) {
        //     // maybe trigger a loading screen
        //     return;
        // }
        if (user?.id !== undefined) navigate("/dashboard");
    }, [navigate, user]);

    return (
        <div className='login'>
            <div className='login__container'>
                <input
                    type='text'
                    className='login__textBox'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='E-mail Address'
                />
                <input
                    type='password'
                    className='login__textBox'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder='Password'
                />
                <Button
                    text={"Login"}
                    fullWidth={true}
                    onClick={login(email, password)}
                    color={"#312F2F"}
                />
                <Button
                    text={"Login with Google"}
                    fullWidth={true}
                    onClick={signInWithGoogle}
                    color={"#EF626C"}
                />
                <div>
                    <Link className='link-color' to='/reset'>
                        Forgot Password
                    </Link>
                </div>
                <div>
                    Don't have an account?{" "}
                    <Link className='link-color' to='/register'>
                        Register
                    </Link>{" "}
                    now.
                </div>
            </div>
        </div>
    );
}
export default Login;
