import { Container, Row } from "react-bootstrap";
import Jumbotron from "../components/Jumbotron/Jumbotron";
import { useContext, useEffect, useState } from "react";
import "../styles/Products.css";
import { CartContext } from "../contexts/ShoppingCart";
import { IProducts } from "../interfaces/IProducts";
import Product from "../components/Product/Product";

const Products = () => {
    const { fetchProducts } = useContext(CartContext);
    const [products, setProducts] = useState<IProducts[]>([]);

    useEffect(() => {
        const fetchData = async () => {
            const x = await fetchProducts();
            setProducts(x);
        }
        fetchData();
    }, [fetchProducts]);

    return (
        <Container fluid>
            <Jumbotron
                header={"Products"}
                description={
                    "A range of embroidered portraits, cushions, blankets."
                }
                button={true}
            />
            <Row className='products-container'>
                {products.length > 0 &&
                    products.map((product, key) => {
                        return <Product key={key} product={product}></Product>;
                    })}
            </Row>
        </Container>
    );
};

export default Products;
